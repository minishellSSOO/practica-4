#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 
#include <string.h>
#include "simula_car.h" 
#define N_COCHES 8

volatile int clasificacionFinal[N_COCHES]; 
volatile int finalCarrera=0; 

// Array de datos de tipo coche_t
coche_t Coches[N_COCHES];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER; 

// Funcion ejecutada por los hilos
void *funcion_coche(coche_t *pcoche)
{
    int aleatorio;
    unsigned int semilla = (pcoche->id) + getpid(); 

    printf("Salida de %s %d\n", pcoche->cadena, pcoche->id);
    
    fflush (stdout);

    aleatorio = rand_r(&semilla) % 10;
 
    sleep(aleatorio);
 
    printf("Llegada de %s %d\n", pcoche->cadena, pcoche->id);
 
    clasificacionFinal[finalCarrera]=pcoche->id;  
    finalCarrera++; 

    pthread_exit(0);
    
}


int main(void)
{
    pthread_t hilosCoches[N_COCHES]; // tabla con los identificadores de los hilos
    
    int i;
    void* ret = NULL;
    pthread_mutex_init(&mutex,NULL);

    printf("Se inicia proceso de creacion de hilos...\n\n");
    printf("SALIDA DE COCHES\n");  

    for (i=0; i<N_COCHES; i++)
    {
        Coches[i].id=i; //asignamos id
        Coches[i].cadena="Coche"; //asignamos la cadena 

        if(pthread_create(&hilosCoches[i],NULL,(void *) &funcion_coche,&Coches[i])!=0){
            perror("pthread_create error.\n");
        }            
    }

    printf("Proceso de creacion de hilos terminado\n\n");

    for (i=0; i<N_COCHES; i++) 
    {
        pthread_mutex_lock(&mutex);

        if(pthread_join(hilosCoches[i],&ret)!=0){ 
            perror("pthread_join.\n");
        }

        pthread_mutex_unlock(&mutex);             
    }

    if(pthread_mutex_destroy(&mutex)!=0){
	    perror("pthread_mutex_destroy error\n");
    }

    printf("Todos los coches han LLEGADO A LA META \n\n");

    printf("<<  CLASIFICACIÓN  >>\n");
    printf("---------------------\n");   

    for (i=0; i<N_COCHES;i++)
    {
	    printf(" Posición %d: %s %d\n",(i+1),Coches->cadena,clasificacionFinal[i]);		
    }      
    printf("---------------------\n");
    return 0;
}